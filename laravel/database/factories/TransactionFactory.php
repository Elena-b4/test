<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Account;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'account_id' => Account::factory(),
            'value' => fake()->randomFloat(2, -100000, 100000),
            'transaction_date' => fake()->dateTimeBetween('-10 years')->format('Y-m-d'),
        ];
    }
}
