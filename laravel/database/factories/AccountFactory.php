<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'balance' => fake()->randomFloat(2),
            'daily_withdrawal_limit' => fake()->randomFloat(2),
            'active' => fake()->boolean(80),
            'account_type' => fake()->numberBetween(1,2),
            'create_date' => fake()->dateTime(),
        ];
    }
}
