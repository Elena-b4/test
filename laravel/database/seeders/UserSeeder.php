<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Account::factory()
            ->for(User::factory()->create())
            ->hasTransactions(5)
            ->create();

        Account::factory()
            ->for(User::factory()->create())
            ->hasTransactions(7)
            ->create();

        User::factory()
            ->count(3)
            ->create();
    }
}
