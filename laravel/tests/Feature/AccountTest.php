<?php

namespace Tests\Feature;

use App\Models\Account;
use Tests\TestCase;

class AccountTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBlockAccount(): void
    {
        $account = Account::factory()->create();
        $response = $this->json('POST', 'api/account/block', ['account_id' => $account->id]);

        $response->assertContent('"Account was blocked successfully!"');

        $response = $this->json('POST', 'api/account/block', ['account_id' => $account->id]);

        $response->assertContent('"Account is already blocked!"');
    }

    public function testGetBalanceThrottled()
    {
        $account = Account::factory()->create();

        for ($i = 0; $i < 4; ++$i) {
            $response = $this->withHeader('Account-id', $account->id)->get('api/account/balance');
        }

        $response->assertStatus(429);
    }

    public function testCheckBlockIpMiddleware()
    {
        $account = Account::factory()->create();

        $response = $this->json('POST', 'api/account/block', ['account_id' => $account->id], ['REMOTE_ADDR' => 'whitelist-ip-1']);

        $response->assertStatus(403);
        $response->assertJsonFragment(['You are restricted to access the site.']);
    }

}
