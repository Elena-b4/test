<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AccountResource
 *
 * @OA\Schema(
 *  @OA\Xml(name="AccountResource")
 * )
 */
class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @OA\Property(format="int(64)", title="account_type", default="1", description="account_type", property="account_type"),
     * @OA\Property(format="bool", title="active", default="true", description="active", property="active"),
     * @OA\Property(format="float", title="daily_withdrawal_limit", default="311313", description="daily_withdrawal_limit", property="daily_withdrawal_limit"),
     * @OA\Property(format="float", title="balance", default="121212", description="balance", property="balance"),
     * @OA\Property(format="string", title="user_id", default="gv-2367we-f23", description="user_id", property="user_id"),
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'user_id' => $this->user_id,
            'balance' => $this->balance,
            'daily_withdrawal_limit' => $this->daily_withdrawal_limit,
            'active' => $this->active,
            'account_type' => $this->account_type,
        ];
    }
}
