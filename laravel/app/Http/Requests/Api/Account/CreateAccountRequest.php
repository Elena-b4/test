<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Account;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateAccountRequest
 *
 * @OA\Schema(
 *  @OA\Xml(name="CreateAccountRequest")
 * )
 */
class CreateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    //для user_id default укажите uuid из бд, чтобы проверить 'Try it out' на фронте
    /**
     * @OA\Property(format="string", title="user_id", default="ea475244-b4cd-4abf-9932-98f5be1cba8e", description="user uuid", property="user_id"),
     * @OA\Property(format="int", title="account_type", default="1", description="account type", property="account_type"),
     * @OA\Property(format="float", title="balance", default="12313", description="balance", property="balance"),
     * @OA\Property(format="float", title="daily_withdrawal_limit", default="12313", description="daily_withdrawal_limit", property="daily_withdrawal_limit"),
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
                'string',
                'exists:users,id',
            ],
            'account_type' => [
                'required',
                'int',
            ],
            'balance' => [
                'required',
                'numeric',
            ],
            'daily_withdrawal_limit' => [
                'required',
                'numeric',
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'user_id.exists' => 'Пользователь не существует.',
        ];
    }
}
