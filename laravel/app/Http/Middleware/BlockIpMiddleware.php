<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BlockIpMiddleware
{
    //Добавьте ниже свой ip, чтобы проверить :)
    public array $blockIps = ['whitelist-ip-1', 'whitelist-ip-2'];

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse|JsonResponse
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse|JsonResponse
    {
        if (in_array($request->ip(), $this->blockIps)) {
            abort(403, 'You are restricted to access the site.');
        }

        return $next($request);
    }
}
