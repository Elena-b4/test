<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Test OpenApi Documentation",
 *      description="Описание API для тестового задания",
 * )
 *
 * @OA\Server(
 *      url="/api",
 *      description="api"
 * )
 *
 * @OA\Tag(
 *     name="Account",
 *     description="Аккаунт"
 * )
 *
 * @OA\PathItem(path="/api")
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
