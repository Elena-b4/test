<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Account\CreateAccountRequest;
use App\Http\Resources\AccountResource;
use App\Models\Account;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CreateAccountController extends Controller
{
    /**
     * @OA\Post(
     *      path="/account",
     *      operationId="createAccount",
     *      tags={"Account"},
     *      summary="Создание аккаунта",
     *      description="Создает аккаунт",
     * @OA\RequestBody(
     *      @OA\JsonContent(
     *          ref="#/components/schemas/CreateAccountRequest"
     *      ),
     *  ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *     @OA\JsonContent(ref="#/components/schemas/AccountResource")
     *       ),
     *     )
     */
    public function __invoke(CreateAccountRequest $request): JsonResponse
    {
        try {
            $data = $request->validated();
            $data['create_date'] = Carbon::now();
            $data['active'] = true;

            $account = Account::create($data);

            return new JsonResponse(new AccountResource($account));
        } catch (ModelNotFoundException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
