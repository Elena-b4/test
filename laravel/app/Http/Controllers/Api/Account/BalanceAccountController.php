<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BalanceAccountController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        $accountId = $request->header('Account-id');

        if (null === $accountId) {
            return new JsonResponse('Account ID cannot be null', 404);
        }

        $account = Account::find($accountId);

        if (!$account) {
            return new JsonResponse('Account not found', 404);
        }

        return new JsonResponse(['balance' => $account->balance]);
    }
}
