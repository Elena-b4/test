<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Account\BlockAccountRequest;
use App\Models\Account;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class BlockAccountController extends Controller
{
    public function __invoke(BlockAccountRequest $request): JsonResponse
    {
        try {
            $data = $request->validated();
            $account = Account::find($data['account_id']);

            if (! $account->active) {
                return new JsonResponse('Account is already blocked!', 403);
            }

            $account->update(['active' => false]);

            return new JsonResponse('Account was blocked successfully!');
        } catch (ModelNotFoundException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
