<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Account;

use App\Exceptions\UpdateBalanceException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Account\UpdateBalanceAccountRequest;
use App\Models\Account;
use App\Repository\BalanceAccountRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UpdateBalanceAccountController extends Controller
{
    public function __construct(
        private BalanceAccountRepository $balanceAccountRepository,
    )
    {
    }

    public function __invoke(UpdateBalanceAccountRequest $request): JsonResponse
    {
        try {
            $data = $request->validated();
            $account = Account::find($data['account_id']);

            if (!$account->active) {
                return new JsonResponse('You cannot use inactive account.', 403);
            }

            $this->balanceAccountRepository->updateBalance($account, $data);

            return new JsonResponse('Balance was updated successfully!');
        } catch (UpdateBalanceException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        } catch (ModelNotFoundException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
