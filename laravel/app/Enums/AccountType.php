<?php

declare(strict_types=1);

namespace App\Enums;

class AccountType
{
    public const NATURAL = 1;
    public const LEGAL = 2;
}
