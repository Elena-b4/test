<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory, UsesUuid;

    public $timestamps = false;

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
}
