<?php

declare(strict_types=1);

namespace App\Repository;

use App\Exceptions\UpdateBalanceException;
use App\Models\Account;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BalanceAccountRepository
{
    /**
     * @throws UpdateBalanceException
     */
    public function updateBalance(Account $account, array $data): void
    {
        DB::beginTransaction();
        try {
            $account->balance += $data['sum'];
            $account->save();

            $transaction = new Transaction();
            $transaction->account_id = $data['account_id'];
            $transaction->value = $data['sum'];
            $transaction->transaction_date = Carbon::now();
            $transaction->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new UpdateBalanceException($exception->getMessage());
        }
    }
}
