<?php

declare(strict_types=1);

use App\Http\Controllers\Api\Account;
use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'account', 'middleware' => ['blockIP']], function () {
    /** Ограничение по количеству запросов на получение текущего счета в день(без дополнительных таблиц) */
    Route::get('/balance', Account\BalanceAccountController::class)->middleware('throttle:3,1440');
    Route::get('/transactions', Account\TransactionsAccountController::class);
    Route::post('/', Account\CreateAccountController::class)->name('account');
    Route::post('/block', Account\BlockAccountController::class);
    Route::put('/balance', Account\UpdateBalanceAccountController::class);
});

