# Test



## Инструкция для запуска

```
git clone https://gitlab.com/Elena-b4/test.git
```
```
cd docker/
```
Создать .env в docker/
```
PROJECT_NAME=test
NGINX_PORT=94

DB_CONNECTION=pgsql
DB_HOST=pgsql
DB_PORT=5430
DB_DATABASE=postgres
DB_USERNAME=test_user
DB_PASSWORD=test_password
```
Далее билд и запуск контейнеров
```
docker-compose up -d
```
Cоздать .env для проекта в папке laravel
```
DB_CONNECTION=pgsql
DB_HOST=pgsql
DB_PORT=5432
DB_DATABASE=postgres
DB_USERNAME=test_user
DB_PASSWORD=test_password
```
Заходим в контейнер php (из test/docker/)
```
docker-compose exec php-fpm bash
```
Установка зависимостей
```
composer install
```
Запуск миграций
```
php artisan migrate
```
Запуск сидов. Создаст несколько пользователей, их аккаунты и транзакции
```
php artisan db:seed
```
Генерация документации swagger, посмотреть можно по урлу /api/documentation/
```
php artisan l5:generate
```
Запуск тестов
```
php artisan test 
```
Для тестирования в Postman необходимо сделать импорт файла test.import_collection.json, который лежит в корне проекта. 
